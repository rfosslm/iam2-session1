#!/bin/bash

if [ "$1" = "" ]; then
echo ' You did not provide an argument.'
    exit 1
else

    if [ ! -f "$1" ]; then
    echo "$1 does not exist"
    exit 1
    fi  
    exit 0
fi